$(document).ready(function () {
        $('#search').keyup(function () {
            var value_search = $('#search');
           if(document.getElementById("search").value !== ""){
                $.ajax({
                        method: 'GET',
                        url: '/nations/api/search/' + value_search.val(),
                        success: function (data) {
                            $('#nations tbody > tr').remove();
                            $.each(data, function (index, c) {
                                $('#nations tbody').append(
                                    '<tr>' +
                                    '   <td>' + c.description + '</td>' +
                                    '   <td>' +
                                    '       <a class="btn btn-block btn-warning" href="/nation/edit/' + c.id + '">Editar</a>' +
                                    '   </td>' +
                                    '</tr>'
                                );
                            });
                        },
                        error: function () {
                            alert("Houve um erro na requisição.");
                        }

                });
            }else if(document.getElementById("search").value.length===0){
                $.ajax({
                        method: 'GET',
                        url: '/nations/api/all',
                        success: function (data) {
                            $('#nations tbody > tr').remove();
                            $.each(data, function (index, c) {
                                $('#nations tbody').append(
                                    '<tr>' +
                                    '   <td>' + c.description + '</td>' +
                                    '   <td>' +
                                    '       <a class="btn btn-block btn-warning" href="/nation/edit/' + c.id + '">Editar</a>' +
                                    '   </td>' +
                                    '</tr>'
                                );
                            });
                        },
                        error: function () {
                            alert("Houve um erro na requisição.");
                        }
                });

           }
        });

});